/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2newox;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab2NewOX {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentplayer = 'X';
    static boolean gameended ;
    static int row, col;
    static boolean pcon;

    static void printWelcome() {
        System.out.println("Welcome to OX");

    }

    static void printtable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }

    }

    static void playerturn() {
        System.out.println(currentplayer + " turn");
    }

    static void inputrowcol() {
        Scanner kb = new Scanner(System.in);
        while (!gameended) {
            System.out.print("Please input row,col:");
            row = kb.nextInt()-1;
            col = kb.nextInt()-1;
            if (table[row][col] != '-') {
                System.out.println("Invalid move");
                continue;
            }
            table[row][col] = currentplayer;
            break;
        }

    }

    static void switchplayer() {
        if (currentplayer == 'X') {
            currentplayer = 'O';
        } else {
            currentplayer = 'X';
        }
    }

    static void checkwinner() {
         if(isWinner()){
               gameended= true;
               System.out.println(currentplayer+" is a winner!");
               System.out.println("Result");
               printtable();
               playcontinue();
               
               
               }
            if(isTie()){
                gameended= true;
               System.out.println("Tie No one win");
               System.out.println("Result");
                printtable();
                playcontinue();
                
            }

    }

    static boolean isWinner() {
        if (rowcheck()|| colcheck()|| diagonalcheck()) {
            return true;
        }
        return false;
    }

    static boolean isTie() {
        if(checkboardfull()){
        return true;
        }
        return false;
    }

    static boolean rowcheck() {
        for (int row = 0; row < 3; row++) {
            if (table[row][0] == currentplayer && table[row][1] == currentplayer && table[row][2] == currentplayer){
                return true;
            }
        }
        return false;
    }

    static boolean colcheck() {
        for (int col= 0; col < 3; col++) {
            if (table[0][col] == currentplayer && table[1][col] == currentplayer && table[2][col] == currentplayer) {
                return true;
            }
        }
        return false;
    }

    static boolean diagonalcheck() {
        if (table[0][0] == currentplayer && table[1][1] == currentplayer && table[2][2] == currentplayer) {
            return true;
        }
        if (table[2][0] == currentplayer && table[1][1] == currentplayer && table[0][2] == currentplayer) {
            return true;
        }
        return false;
    }
    static boolean checkboardfull(){
    for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (table[row][col] == '-') {
                    return false;
                }
            }
        }
        return true;}
    static void playcontinue(){
        Scanner kb = new Scanner(System.in);
        String Continue;
        System.out.println("Please input continue or exit: ");
        Continue = kb.nextLine();
        if(Continue.equals("continue")){
           pcon = true;
        }
        if(Continue.equals("exit")){
          pcon = false;
        }
    }
    static void resettable(){
      for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j]='-';
            }
        }
    }
    static void resetturn(){
     currentplayer = 'X';    
    }

    public static void main(String[] args) {
        printWelcome();
        pcon = true;
        while(pcon){
            resettable();
            resetturn();
            gameended = false;
           while (!gameended) {
            printtable();
            playerturn();
            inputrowcol();
            checkwinner();
            switchplayer();
           }
        
          
 
        }

    }
}
